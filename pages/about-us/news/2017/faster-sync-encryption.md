@title = 'Faster sync, faster encryption'
@author = 'drebs'
@posted_at = '2017-09-15'
@more = false
@preview_image = '/img/pages/soledad_benchmarks.png'

We are hard at work improving Bitmask's encryption and synchronization times. Benchmarking is showing excellent results. In order to improve Bitmask's performance for email, recent work on the sync-able and encrypted [Soledad](https://leap.se/soledad) data-store has been to adapt it to better handle binary e2e-encrypted data (what we are calling "blobs"). Work in the last months has been in the direction of making the [Soledad](https://leap.se/soledad) "blobs" support more reliable, better documented and tested, and to make speed improvements visible.

The [next stable version](https://0xacab.org/leap/soledad/wikis/2017-roadmap) of [Soledad](https://leap.se/soledad) will bring full "blobs" support, with transfer and crypto improvements, as well as data persistence guarantees. This will pave the way to modifications needed in Bitmask Mail that will take advantage of this new system, thus improving the overall speed and reliability of the whole server and client-side LEAP mail system.

